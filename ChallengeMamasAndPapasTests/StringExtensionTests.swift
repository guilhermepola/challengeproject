//
//  StringExtensionTests.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 14/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
import Nimble
import Quick

@testable import ChallengeMamasAndPapas

class StringExtensionTests: QuickSpec {
    
    var textWithSpecialCharacter = String()
    var textWithoutSpecialCharacter = String()
    
    override func spec(){

        
        describe("Testing extension of Strings") {
            
         

            beforeEach{
            
                self.textWithSpecialCharacter = "<strong>We love this because...</strong>"
                self.textWithoutSpecialCharacter = "We love this because..."

                
            }
            it("Should be returned just text without web character", closure: {
               
                expect(self.textWithSpecialCharacter.htmlTottributedString?.string) == self.textWithoutSpecialCharacter
                expect(self.textWithSpecialCharacter.htmlToString) == self.textWithoutSpecialCharacter

                
                
            })
            
        }
        
    }
}

