//
//  FeedCollectionViewControllerTests.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 14/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
import Nimble
import Quick
import ObjectMapper
@testable import ChallengeMamasAndPapas

class FeedCollectionViewControllerTests: QuickSpec {
    let storyboard = UIStoryboard(name: "Feed", bundle: nil)
    var collectionViewController: FeedCollectionViewController!
    var viewModel = FeedViewModel()
    
    override func spec(){
        beforeEach {
            let testBundle = Bundle(for: type(of: self))
            let mock = Mocks(fileName: "feed", bundle: testBundle)
            let objects = Mapper<Products>().map(JSON: mock.jsonDic)
            
            self.collectionViewController = self.storyboard.instantiateViewController(withIdentifier: String(describing: FeedCollectionViewController.self)) as! FeedCollectionViewController
            self.viewModel.products = objects
            self.collectionViewController.viewModel = self.viewModel
            self.collectionViewController.collectionView?.dataSource = self.collectionViewController
            
            
        }
        describe("Testing FeedCollectionViewController") {
            
            it("hould have the expected number of items", closure: {
                let collectionView = self.collectionViewController.collectionView
                let count = self.collectionViewController.collectionView(collectionView!, numberOfItemsInSection: 1)
                expect(count).to(equal(20))
            })
            
            it("should have the expected number of section"){
                let collectionView = self.collectionViewController.collectionView
                let count = self.collectionViewController.numberOfSections(in: collectionView!)
                expect(count).to(equal(1))
            }
            
            it("Should return my cell", closure: {
                let collectionView = self.collectionViewController.collectionView
                
                let cell = self.collectionViewController.collectionView(collectionView!, cellForItemAt: IndexPath(item: 0, section: 0)) as! FeedCollectionViewCell
                let product = self.viewModel.product(at: 0)
                expect(cell.nameLabel.text) == product?.title
                expect(cell.fullPriceLabel.attributedText) == product?.price
                expect(cell.priceWithDiscountLabel.text) == product?.specialPrice
                expect(cell.discountLabel.text) == product?.discount

                expect(cell).to(beAKindOf(FeedCollectionViewCell.self))
                
                
                
            })
            
        }
        
    }
}
