//
//  ProductsTests.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 14/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
import Nimble
import Quick
import ObjectMapper

@testable import ChallengeMamasAndPapas

class ProductsTests: QuickSpec {
    
    var products: Products!

    override func spec(){
        
        describe("Testing Products Model") {
            
            beforeEach {
                let testBundle = Bundle(for: type(of: self))
                let mock = Mocks(fileName: "feed", bundle: testBundle)
                self.products = Mapper<Products>().map(JSON: mock.jsonDic)
            }
            
            it("Should create model for user with Success", closure: {
                expect(self.products).toNot(beNil())
                expect(self.products.hits).toNot(beNil())

            })
            it("should has all proprieties required", closure: {
                
                expect(self.products.hits?.count) == 20
                expect(self.products.hits?[0].name) == "Millie & Boris - Musical Cot Mobile"
                expect(self.products.hits?[0].discounted) == "no"
                expect(self.products.hits?[0].badges?.discount).to(beNil())
                expect(self.products.hits?[0].price) == 199
                expect(self.products.hits?[0].specialPrice) == 185
                
            })
            
        }
        
    }
}

