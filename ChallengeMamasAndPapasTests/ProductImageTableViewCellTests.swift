//
//  ProductImageTableViewCellTests.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 14/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
import Nimble
import Quick
import ObjectMapper
@testable import ChallengeMamasAndPapas

class ProductImageTableViewCellMock: ProductImageTableViewCell {

    let stubbedProductImageView: UIImageView! = UIImageView()
    override var productImageView: UIImageView!{
        get {
            return stubbedProductImageView
        }
        set {}
    }
    
    let stubbedNameaLabel: UILabel! = UILabel()
    override var nameLabel: UILabel!{
        get {
            return stubbedNameaLabel
        }
        set {}
    }
    let stubbedDescriptionTextView: UITextView! = UITextView()
    override var descriptionTextView: UITextView!{
        get {
            return stubbedDescriptionTextView
        }
        set {}
    }
    
    
}


class ProductImageTableViewCellTests: QuickSpec {
    
    var productImageTableViewCellMock = ProductImageTableViewCellMock()
    var viewModel: ProductViewModel?
    override func spec(){
        
        beforeEach{
            let testBundle = Bundle(for: type(of: self))
            let mock = Mocks(fileName: "feed", bundle: testBundle)
            let objects = Mapper<Products>().map(JSON: mock.jsonDic)
            let hits = objects?.hits?[0]
            self.viewModel = ProductViewModel(product: hits!)

        }
        describe("Unit testing ProductImageTableViewCellTests") {
            
            it("Should be put value in this Outlets", closure: {
                self.productImageTableViewCellMock.configurate(viewModel: self.viewModel!)
                expect(self.productImageTableViewCellMock.nameLabel.text) == self.viewModel?.title
                expect(self.productImageTableViewCellMock.descriptionTextView.text) == self.viewModel?.description

            })
            
            it("Should Image be nil", closure: {
                expect(self.productImageTableViewCellMock.productImageView.image).toNot(beNil())

                self.productImageTableViewCellMock.prepareForReuse()
                
                expect(self.productImageTableViewCellMock.productImageView.image).to(beNil())


                
            })
        }
        
    }
}

