//
//  ProductSizeTableViewCellTests.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 14/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
import Nimble
import Quick
import ObjectMapper

@testable import ChallengeMamasAndPapas

class ProductSizeTableViewCellMock: ProductSizeTableViewCell {
 
}

class ProductSizeTableViewCellTests: QuickSpec {
    var productSizeTableViewCellMock = ProductSizeTableViewCellMock()
    var viewModel: ProductViewModel?
    override func spec(){
        
        beforeEach{
            let testBundle = Bundle(for: type(of: self))
            let mock = Mocks(fileName: "feed", bundle: testBundle)
            let objects = Mapper<Products>().map(JSON: mock.jsonDic)
            let hits = objects?.hits?[0]
            self.viewModel = ProductViewModel(product: hits!)
            
        }
        describe("Unit testing ProductSizeTableViewCellMTests") {
            
            it("Should be put value in this Outlets", closure: {

            })
            
        }
        
    }
}
