//
//  Mock.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 14/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//


import Foundation
struct Mocks {
    
    var jsonDic = [String: Any]()
    var arrayJSON = [[String: Any]]()
    
    init(fileName: String, bundle: Bundle){
        
        
        guard let path = bundle.path(forResource: fileName, ofType: "json") else {
            fatalError("fileName.json not found")
        }
        guard let jsonString = try? NSString(contentsOfFile: path, encoding: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert fileName.json to String")
        }
        
        
        guard let jsonData = jsonString.data(using: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert fileName.json to NSData")
        }
        
        guard let json = try? JSONSerialization.jsonObject(with: jsonData, options: []) else {
            fatalError("Unable to convert fileName.json to JSON dictionary")
        }
        guard let jsonDictionary = json as? [String:Any] else{
            if let arrayJson = json as? [[String: Any]]{
                arrayJSON = arrayJson
                
            }
            return
        }
        jsonDic = jsonDictionary
        
        
    }
    
    
    
}
