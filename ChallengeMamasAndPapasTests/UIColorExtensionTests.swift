//
//  UIColorExtensionTests.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 14/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
import Nimble
import Quick
@testable import ChallengeMamasAndPapas

class UIColorExtensionTests: QuickSpec {
    
    override func spec(){
        
        describe("Testing UIColorExtension") {
            
            it("Verify is the same gold color", closure: {
                let colorGold = UIColor.init(red: 221, green: 200, blue: 0)
                expect(colorGold) == UIColor.mammasAndPapasColor

            })
            
        }
        
    }
}
