//
//  EmptyCellTableViewCell.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 15/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//


import Nimble
import Quick

@testable import ChallengeMamasAndPapas
class EmptyCellTableViewCellMock: EmptyCellTableViewCell{
    
    let stubbedLabelEmpty: UILabel! = UILabel()
    override var labelEmpty: UILabel!{
        get {
            return stubbedLabelEmpty
        }
        set{}
    }
    
    let sttubbedImageViewEmpty = UIImageView()
    override var imageViewEmpty: UIImageView! {
        get {
            return sttubbedImageViewEmpty
        }
        set{}
    }
}

class EmptyCellTableViewCellTests: QuickSpec {
    
    let mockCell = EmptyCellTableViewCellMock()
    
    
    override func spec() {
        beforeEach {
            
        }
        describe("Unit testing in EmptyCell") {
            it("should be put value in this Outlets", closure: {
                self.mockCell.configure(message: "No Message", image: #imageLiteral(resourceName: "empty-Image"))
                expect(self.mockCell.stubbedLabelEmpty.text) == "No Message"
                expect(self.mockCell.sttubbedImageViewEmpty.image) == #imageLiteral(resourceName: "empty-Image")
            })
        }
        
    }
    
}
