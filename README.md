/*:
This Challenge contains the task definition and example code for the [MamasAndPapas](http://mamasandpapas.ae).

----

**Jump to:**

- [Installation](#installation)
- [Prerequisites](#prerequisites)
- [Architecture](#architecture)
- [Running Tests](#runningUnitTests)
- [Coverage Tests](#coverageTests)

# Installation

## Prerequisites

- [Swift](https://swift.org) 3.1
- [Xcode](https://developer.apple.com/xcode/) 8.3.x - we recommend downloading in the App Store
- [CocoaPods](https://guides.cocoapods.org/using/getting-started.html) 1.1.2

After cloned or downloaded the project and open the directory in the same level of the file ChallengeMamasAndPapas.xcodeproj RUN:
```
pod install
```

Always that you want open the project, you have to open the ChallengeMamasAndPapas.xcworkspace

## Architecture

Using the MVVM (Moldel-View-ViewModel), SOLID and OOP, avoid the Massive View Controller.
Also I create a layer with the Networking and some concepts of POP (Protocol Oriented Programmer) like when I register and create a cell

## RunningUnitTests

There are around 60% unit testing in this project, I use Nimble and Quick to do the BDD and Mockingjay to mock the request in the API
* Cmmd + U

## coverageTests

* Selected tests target
* Cmmd + U
* Cliced in Report Navigation

*/
