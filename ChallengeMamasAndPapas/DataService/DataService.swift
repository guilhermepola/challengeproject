//
//  DataService.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 15/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
import ObjectMapper

private let kBag = "bag"


class DataService {

    var hits = [Bag]()
    //MARK: Singleton
    static var sharedInstance: DataService?
    static func getInstance() -> DataService{
        if sharedInstance == nil{
            sharedInstance = DataService()
            sharedInstance?.retrieveBags()
            
        }
        return sharedInstance!
    }
    
    func storeHit(bag: Bag?){
        let userDefaults = UserDefaults.standard
        guard let bag = bag else {return}
        self.hits.append(bag)
        userDefaults.set(self.hits.toJSON(), forKey: kBag)
    }
    
    func removeHit(at index: Int){
        let userDefaults = UserDefaults.standard
        self.hits.remove(at: index)
        userDefaults.set(self.hits.toJSON(), forKey: kBag)
    }
    
    func retrieveBags(){
        let userDefaults = UserDefaults.standard
        if let hits = userDefaults.object(forKey: kBag) as? [[String: Any]]{
            let newHit = Mapper<Bag>().mapArray(JSONArray: hits)
            self.hits = newHit
        }
    }
    
    
}
