//
//  EmptyCelTableViewCell.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 15/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit

class EmptyCellTableViewCell: UITableViewCell {

    @IBOutlet weak var labelEmpty: UILabel!
    @IBOutlet weak var imageViewEmpty: UIImageView!
    @IBOutlet weak var buttonEmpty: UIButton!




    func configure(message: String, image: UIImage) {
        labelEmpty.text = message
        imageViewEmpty.image = image
    }



}
