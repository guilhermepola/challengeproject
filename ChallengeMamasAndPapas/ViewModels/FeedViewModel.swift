//
//  FeedsViewModel.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import Foundation

class FeedViewModel {
    
    var products: Products?
    var nextPage: Int = 0
    
    
    var numberOfProducts: Int {
        guard let hits = products?.hits else{
            return 0
        }
        return hits.count
    }
    
    func product(at index: Int) ->  ProductRepresentable? {
        guard let hits = products?.hits else{
            return nil
        }
        return ProductViewModel(product: hits[index])
    }
    
    func hit(at index: Int) -> Hits?{
        return products?.hits?[index]
    }
    
    func loadProducts(completion: @escaping() -> Void){
        nextPage += 1
        APIProducts(nextPage: nextPage).post { result in
            switch result{
            case .success(let resultProducts):
                guard self.products != nil else {
                    self.products = resultProducts
                    completion()
                    return
                }
                self.products?.hits?.append(contentsOf: resultProducts.hits ?? [])
                completion()
            case .failure(_):
                self.nextPage -= 1
                completion()
                
            }
        }
    }

}
