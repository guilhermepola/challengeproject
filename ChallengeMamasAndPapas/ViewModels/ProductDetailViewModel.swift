//
//  ProductDetailViewModel.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 14/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit

class ProductDetailViewModel {
    var items = [ProductDetailViewModelItem]()
    var hit: ProductRepresentable?
    var product: Hits?
    
    func loadProduct(slug: String, completion: @escaping() -> Void){
        
        APIProduct(slug: slug).get { (result) in
            switch result{
            case .success(let product):
                self.product = product
                self.hit = ProductViewModel(product: product)
                completion()
            case .failure(_):
                completion()
                break
            }
            
        }
    }
    
    func getProductById(id: String) {
    
        self.hit = ProductViewModel(product: (self.product?.relatedProductsLookup?[id])!)
        
    }
    
}


