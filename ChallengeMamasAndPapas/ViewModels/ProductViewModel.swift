//
//  FeedViewModel.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import Foundation
import Kingfisher
struct ProductViewModel: ProductRepresentable {

    private var product: Hits
    
    init(product: Hits){
        self.product = product
    }
    
    var heightCell: CGFloat = 266.0
    
    var specialPrice: String{
        guard let specialPrice = self.product.specialPrice else {
            return price.string
        }
        return "\(specialPrice) AED"
    }
    var price: NSAttributedString{
    
        guard let price = self.product.price else {
            return NSAttributedString(string: "", attributes: [NSStrikethroughStyleAttributeName: NSUnderlineStyle.styleSingle])
        }
        
        let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: "\(price) AED")
        attributeString.addAttribute(NSBaselineOffsetAttributeName, value: 0, range: NSMakeRange(0, attributeString.length))
        attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 1, range: NSMakeRange(0, attributeString.length))
        
        return attributeString
    }
    
    var discount: String{
        guard let discount = self.product.badges?.discount else {
            return ""
        }
        return "\(discount)% Off"
    }

    var title: String{
        return self.product.name ?? "No Title"
    }
    var imageProduct: URL?{
        return URL(string: (APIPath.baseURLToImage.path + (self.product.image ?? "0")))
    }
    var category: String{
        return self.product.slug ?? "No Category"
    }
    
    var listSize: [String] {
        return self.self.product.sizesInStock ?? []
    }
    var description: String{
        return self.product.description?.htmlToString ?? ""
    }
    var quantityStock: Int {
        return self.product.stock?.maxAvailableQty ?? 0
    }
    

}
