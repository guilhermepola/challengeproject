//
//  APIPath.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 24/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import Foundation
enum APIPath {
    case baseURL
    case baseURLToImage
    case products
    case product
}
