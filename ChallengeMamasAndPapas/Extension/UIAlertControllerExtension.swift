//
//  UIAlertControllerExtension.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 15/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
private let kOK = "OK"

extension UIAlertController {
    
    static func alertWithTitle(title: String, message: String, onDismiss: ((Void) -> Void)? = nil) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionCancel = UIAlertAction(title: kOK, style: .cancel) { (alert) in
            onDismiss?()
        }
        alertController.addAction(actionCancel)
        
        return alertController
    }
}
