//
//  AppDelegateExtension.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//
import UIKit

extension AppDelegate {
    
    func configureAppStyling() {
        styleNavigationBar()
    }
    
    func styleNavigationBar() {
        
        UINavigationBar.appearance().barTintColor = .mammasAndPapasColor
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        
    }
}
