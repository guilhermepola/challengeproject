//
//  UIableViewCellExtension.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit

extension UITableViewCell: ReusableView, NibLoadableView{}
