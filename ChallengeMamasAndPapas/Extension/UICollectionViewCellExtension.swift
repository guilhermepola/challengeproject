//
//  UICollectionViewCellExtension.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit

extension UICollectionViewCell: NibLoadableView, ReusableView {
    
}
