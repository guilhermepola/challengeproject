//
//  AlertMessagesExtension.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 24/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import Foundation

extension AlertMessages {
    var string: String {
        switch self {
        case .noSizeSelectTitle: return "No Size selected"
        case .noSizeSelectMessage: return "Please, select a size"
        case .productAddedTitle: return "Product add 🎉"
        case .productAddedMessage: return "A product was added in your bag"
        case .noProductBag: return "No Product in the Bag :("
        }
    }
}
