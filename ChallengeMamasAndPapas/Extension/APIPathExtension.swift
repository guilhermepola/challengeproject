//
//  APIPathExtension.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 24/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import Foundation

extension APIPath : Path {
    var path: String {
        switch self {
        case .baseURL: return "https://www.mamasandpapas.ae/"
        case .baseURLToImage: return "https://prod4.atgcdn.ae/media/catalog/product"
        case .products: return "search/full/"
        case .product: return "product/findbyslug/"
        }
    }
}
