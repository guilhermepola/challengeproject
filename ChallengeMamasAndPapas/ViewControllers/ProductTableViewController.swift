//
//  ProductTableViewController.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit


class ProductTableViewController: UITableViewController, AddBagProtocol, QuantityProtocol {
    
    var bag = Bag()
    var productDetailViewModel = ProductDetailViewModel()
    var slug = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.productDetailViewModel.loadProduct(slug: self.slug) {
            self.tableView.reloadData()
        }
        
        self.tableView.register(class: ProductImageTableViewCell.self)
        self.tableView.register(class: ProductSizeTableViewCell.self)
        self.tableView.register(class: ProductQuantityTableViewCell.self)
        self.tableView.register(class: AddBagTableViewCell.self)
        self.tableView.register(class: LoadingTableViewCell.self)

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.productDetailViewModel.hit == nil ? 1: 4
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

   
        if self.productDetailViewModel.hit == nil{
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as LoadingTableViewCell
            return cell
        }
        
        switch indexPath.row {
        case 0:
           let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as ProductImageTableViewCell
           cell.configurate(viewModel: self.productDetailViewModel.hit)
            return cell
            
        case 1 :
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as ProductSizeTableViewCell
            cell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
          
            return cell
        case 2:
            
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as AddBagTableViewCell
            cell.delegate = self
            bag.quantity = 1
            cell.configure(viewModel: self.productDetailViewModel.hit)

            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as ProductQuantityTableViewCell
            cell.delegate = self
            cell.configure(viewModel: self.productDetailViewModel.hit)
            return cell

        default:
            return UITableViewCell()

        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 484
        case 1:
            return 100
        case 2:
            return 80
        case 3:
            return 80
        default:
            return 0
        }
    }
    
    func addBag() {
        guard bag.size != nil || bag.quantity != 0 else {
            let alertController = UIAlertController.alertWithTitle(title: AlertMessages.noSizeSelectTitle.string, message: AlertMessages.noSizeSelectMessage.string)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        let alertController = UIAlertController.alertWithTitle(title: AlertMessages.productAddedTitle.string, message: AlertMessages.productAddedMessage.string) { () in
            self.navigationController?.popViewController(animated: true)
        }
        bag.name = self.productDetailViewModel.hit?.title
        bag.photoURL = self.productDetailViewModel.hit?.imageProduct
        bag.price = self.productDetailViewModel.product?.specialPrice
        DataService.getInstance().storeHit(bag: bag)
        self.present(alertController, animated: true, completion: nil)
  
    }
    func quantityInPicker(quantity: Int) {
        let cell = self.tableView.cellForRow(at: IndexPath(item: 3, section: 0)) as? ProductQuantityTableViewCell
        cell?.quantityLabel.text = "Quantity \(quantity)"
        bag.quantity = quantity
    }
    
}

extension ProductTableViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let hit = self.productDetailViewModel.hit else{
            return 0
        }
        return hit.listSize.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as ProductSizeCollectionViewCell
        cell.sizeLabel.text = self.productDetailViewModel.hit?.listSize[indexPath.row]
        if cell.isSelected {
            cell.backgroundColor = .gray
        }else {
            cell.backgroundColor = .white

        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 137, height: 60)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        collectionView.cellForItem(at: indexPath)?.backgroundColor = .white
        collectionView.cellForItem(at: indexPath)?.isSelected = false


    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if let productId = (self.productDetailViewModel.product?.configurableAttributes?[0].options?[indexPath.row].simpleProductSkus?[0]) {
                self.productDetailViewModel.getProductById(id: productId)
            }
        
        bag.size = String(describing:self.productDetailViewModel.hit?.quantityStock)
        bag.quantity = 0
        collectionView.cellForItem(at: indexPath)?.isSelected = true

        collectionView.cellForItem(at: indexPath)?.backgroundColor = .gray
        let cellPicker = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! AddBagTableViewCell
        cellPicker.arrayStock = Array(1...(self.productDetailViewModel.hit?.quantityStock)!)
        cellPicker.quantityTextField.text = "0"
        cellPicker.pickerView.reloadAllComponents()
        
    }
}
