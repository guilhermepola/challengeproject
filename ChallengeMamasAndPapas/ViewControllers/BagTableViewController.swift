//
//  BagTableViewController.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 15/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit

class BagTableViewController: UITableViewController {

    var bag = [Bag](){
        didSet{
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(class: BagTableViewCell.self)
        self.tableView.register(class: EmptyCellTableViewCell.self)


    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.bag = DataService.getInstance().hits
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bag.count == 0 ? 1 : self.bag.count
    }
   
    func emptyCell(tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> EmptyCellTableViewCell {
        
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as EmptyCellTableViewCell
        cell.imageViewEmpty.image = #imageLiteral(resourceName: "empty-Image")
        cell.labelEmpty.text =  AlertMessages.noProductBag.string
        
        return cell
        
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if bag.count == 0 {
            return emptyCell(tableView: tableView, cellForRowAtIndexPath: indexPath)

        }
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as BagTableViewCell
        cell.configure(bag: self.bag[indexPath.row])

        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return bag.count == 0 ? self.tableView.frame.height : 120

    }
}
