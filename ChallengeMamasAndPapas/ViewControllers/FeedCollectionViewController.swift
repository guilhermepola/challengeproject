//
//  FeedCollectionViewController.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
import UIScrollView_InfiniteScroll

extension FeedCollectionViewController: SegueHandlerType {
    enum SegueIdentifier: String{
        case segueDetail
    }
}


class FeedCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    var viewModel = FeedViewModel()
    var refreshControll  = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView?.register(class: FeedCollectionViewCell.self)
        settingsRefreshControl()
        getProducts()
        createInfiniteScroll()
    }

    
    override func didReceiveMemoryWarning() {
       super.didReceiveMemoryWarning()
    }
    func getProducts(){
      self.viewModel.loadProducts {
            self.collectionView?.reloadData()
            self.collectionView?.finishInfiniteScroll(completion: nil)
            self.refreshControll.endRefreshing()
        }
    }
    func createInfiniteScroll(){
        self.collectionView?.addInfiniteScroll { (scrollView) -> Void in
            self.getProducts()
        }
    }
    
    func settingsRefreshControl(){
        self.viewModel.nextPage = 0
        self.viewModel.products = nil
        self.refreshControll.addTarget(self, action: #selector(getProducts), for: .valueChanged)
        self.collectionView?.addSubview(refreshControll)
    }



    override func numberOfSections(in collectionView: UICollectionView) -> Int {
       return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfProducts
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as FeedCollectionViewCell
        cell.configure(viewModel: viewModel.product(at: indexPath.row))
    
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.size.width / 2, height: 266)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: .segueDetail, sender: indexPath)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueIdentifier(for: segue) {
            case .segueDetail:
            let vc = segue.destination as? ProductTableViewController
            guard let selectedIndexPath = sender as? IndexPath else { return }
            guard let hit = viewModel.hit(at: selectedIndexPath.item) else { return }
            vc?.slug = hit.slug ?? ""
        }
    }
}
