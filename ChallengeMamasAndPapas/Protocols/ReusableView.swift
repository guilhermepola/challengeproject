//
//  ReusableView.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit

protocol  ReusableView: class{}

extension ReusableView where Self: UIView{

    static var reuseIdentifier: String{
        return String(describing: self)
    }
}
