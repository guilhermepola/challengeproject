//
//  Postable.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

public protocol Postable {
    associatedtype DataType
    associatedtype ErrorMessage
    func post(completion: @escaping (Result<DataType, ErrorMessage>) -> Void)
}
