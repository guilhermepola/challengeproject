//
//  ProductRepresentable.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//
import UIKit
protocol ProductRepresentable {
    
    var title: String { get }
    var category: String { get }
    var price : NSAttributedString { get }
    var specialPrice: String { get }
    var imageProduct: URL? { get }
    var heightCell: CGFloat { get }
    var discount: String { get }
    var description: String { get }
    var listSize: [String] { get }
    var quantityStock: Int { get }
    
    
    
}
