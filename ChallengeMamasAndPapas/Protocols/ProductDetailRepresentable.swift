//
//  ProductDetailRepresentable.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 14/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import Foundation
public enum ProductDetailModelItemType {
    case nameAndPicture, size, quantity
}


protocol ProductDetailViewModelItem {
    var type: ProductDetailModelItemType { get }
    var heightCell: Float { get }
    var rowCount: Int { get }
}

class NameAndPictureViewModel: ProductDetailViewModelItem  {
    
    var rowCount: Int {
        return 1
    }
    var type: ProductDetailModelItemType {
        return .nameAndPicture
    }
    var heightCell: Float{
        return 600
    }
    
}

class SizeViewModel: ProductDetailViewModelItem  {
    
    var rowCount: Int {
        return 1
    }
    var type: ProductDetailModelItemType {
        return .size
    }
    var heightCell: Float{
        return 60
    }

}

class QuantityViewModel: ProductDetailViewModelItem  {
    
    var rowCount: Int {
        return 1
    }
    var type: ProductDetailModelItemType {
        return .quantity
    }
    var heightCell: Float{
        return 100
    }
    
}
