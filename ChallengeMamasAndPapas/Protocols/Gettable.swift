//
//  Gettable.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

public protocol Gettable {
    associatedtype DateTyep
    associatedtype Error
    func get(completion: @escaping (Result<DateTyep, Error>) -> Void)
}
