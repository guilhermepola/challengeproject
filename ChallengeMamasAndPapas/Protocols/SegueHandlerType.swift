//
//  SegueHandlerType.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 24/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

protocol SegueHandlerType {
    
    associatedtype SegueIdentifier: RawRepresentable
}
