//
//  LoadingTableViewCell.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 25/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit

class LoadingTableViewCell: UITableViewCell {

    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    
}
