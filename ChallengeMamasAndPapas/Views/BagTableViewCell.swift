//
//  BagTableViewCell.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 15/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit

class BagTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var imageViewProduct: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    func configure(bag: Bag){
        imageViewProduct.kf.setImage(with: bag.photoURL, placeholder: #imageLiteral(resourceName: "noimage"))
        nameLabel.text = bag.name
        guard let quantity = bag.quantity else{
            return
        }
        quantityLabel.text = "Quantity: \(quantity)"
        guard let price = bag.price else { return }
        priceLabel.text = "Total: \((quantity * price)) AED"
        
    }
    
}
