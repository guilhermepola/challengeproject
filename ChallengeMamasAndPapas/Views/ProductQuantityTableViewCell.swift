//
//  ProductQuantityTableViewCell.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
protocol AddBagProtocol{
    func addBag()
}
class ProductQuantityTableViewCell: UITableViewCell {

    var delegate: AddBagProtocol?

    @IBOutlet weak var priceTotalLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    

    func configure(viewModel: ProductRepresentable?){
        guard let viewModel = viewModel else{ return }

        priceTotalLabel.text = "Price unit: \(viewModel.specialPrice)"
        quantityLabel.text = "Quantity 0"
        
    }
    @IBAction func buy(_ sender: UIButton) {
        delegate?.addBag()
    }
    
}
