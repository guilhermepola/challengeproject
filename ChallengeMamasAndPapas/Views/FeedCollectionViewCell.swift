//
//  FeedCollectionViewCell.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
import Kingfisher

class FeedCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var fullPriceLabel: UILabel!
    @IBOutlet weak var priceWithDiscountLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    func configure(viewModel: ProductRepresentable?){
        guard let viewModel = viewModel else {
            return
        }
        
        self.productImageView.kf.setImage(with: viewModel.imageProduct, placeholder: #imageLiteral(resourceName: "noimage"))
        self.nameLabel.text = viewModel.title
        self.fullPriceLabel.attributedText = viewModel.price
        self.priceWithDiscountLabel.text = viewModel.specialPrice
        self.discountLabel.text = viewModel.discount
 
    }

}
