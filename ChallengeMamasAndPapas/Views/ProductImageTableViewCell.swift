//
//  ProductImageTableViewCell.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit

class ProductImageTableViewCell: UITableViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    
    func configurate(viewModel: ProductRepresentable?){
        guard let viewModel = viewModel else{ return }
        self.productImageView.kf.setImage(with: viewModel.imageProduct, placeholder: #imageLiteral(resourceName: "noimage"))
        self.nameLabel.text = viewModel.title
        self.descriptionTextView.text = viewModel.description
        
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        
        productImageView?.image = nil
    }
    
    
}
