//
//  ProductSizeCollectionViewCell.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit

class ProductSizeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var sizeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        configureLabel()
    }

    func configureLabel(){
        sizeLabel.layer.borderColor = UIColor.black.cgColor
        sizeLabel.layer.borderWidth = 1.0
        
    }

}
