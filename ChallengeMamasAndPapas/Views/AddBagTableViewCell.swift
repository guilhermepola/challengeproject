//
//  AddBagTableViewCell.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 14/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
protocol QuantityProtocol {
    func quantityInPicker(quantity: Int)
}
class AddBagTableViewCell: UITableViewCell, UIPickerViewDataSource, UIPickerViewDelegate {

    var delegate: QuantityProtocol?

    @IBOutlet weak var quantityTextField: UITextField!
    var pickerView = UIPickerView()
    var arrayStock = [Int]()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.quantityTextField.inputView = self.pickerView
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
    }
    
    func configure(viewModel: ProductRepresentable?){
        guard let viewModel = viewModel else{ return }
        arrayStock = Array(1...viewModel.quantityStock)
        self.pickerView.reloadAllComponents()
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayStock.count
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(describing: arrayStock[row])
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.quantityTextField.text = String(arrayStock[row])
        delegate?.quantityInPicker(quantity: arrayStock[row])
    }
    
}
