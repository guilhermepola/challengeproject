//
//  APIProduct.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
import ObjectMapper

private let kSlug = "slug"

class APIProduct: API, Gettable {

    var slug: String
    
    init(slug: String) {
        self.slug = slug
    }
   
    func get(completion: @escaping (Result<Hits, String>) -> Void){
        
        request(method: .get, path: APIPath.product.path, parameters: [kSlug: self.slug]) { (response) in
            
            let errorDescription = Result<Hits, String>.failure("Error")
            
            guard let value = response.result.value as? [String: Any] else{
                
                completion(errorDescription)
                return
                
            }
            
            guard let products = Mapper<Hits>().map(JSON: value) else {
                completion(errorDescription)
                return
            }
            let resultProducts = Result<Hits, String>.success(products)
            
            completion(resultProducts)
        }
        
    
    }

}
