//
//  APIProducts.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
import ObjectMapper


private let kPage = "page"
private let kHits = "hits"
private let kSearchString = "searchString"

final class APIProducts: API, Postable {

    var nextPage: Int
    
    init(nextPage: Int) {
        self.nextPage = nextPage
    }

    func post(completion: @escaping (Result<Products, String>) -> Void) {

        let params = [kPage: nextPage,
                      kHits: 10,
                      kSearchString: "Boy"] as [String : Any]
        
        request(method: .post, path: APIPath.products.path, parameters: params) { (response) in
            
            let errorDescription = Result<Products, String>.failure("Error")
            
            guard let value = response.result.value as? [String: Any] else{
                
                completion(errorDescription)
                return
                
            }
            
            guard let products = Mapper<Products>().map(JSON: value) else {
                completion(errorDescription)
                return
            }
            let resultProducts = Result<Products, String>.success(products)
            
            completion(resultProducts)
        }
        
    }

    
}
