//
//  API.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import Alamofire
import ObjectMapper

class API {

     func request(method: Alamofire.HTTPMethod, path: String, parameters: [String: Any]? = nil, completion: @escaping (DataResponse<Any>) -> Void) {
       
        let URL = APIPath.baseURL.path + path
        Alamofire.request(URL, method: method, parameters: parameters, encoding: URLEncoding.queryString, headers: getHeaders()).validate().responseJSON(completionHandler: completion)
        
    }

    
    func getHeaders () -> HTTPHeaders?{
        
        return nil
    }
    
    
}
