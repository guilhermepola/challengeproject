//
//  Stock.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//


import UIKit
import ObjectMapper

struct Stock{
    var homeDeliveryQty: Int?
    var clickAndCollectQty: Int?
    var maxAvailableQty: Int?
    
}

extension Stock: Mappable{
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        
        homeDeliveryQty <- map["homeDeliveryQty"]
        clickAndCollectQty <- map["clickAndCollectQty"]
        maxAvailableQty <- map["maxAvailableQty"]

    }
    
    
}
