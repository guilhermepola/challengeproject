//
//  Bag.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 15/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
import ObjectMapper

struct Bag {
    var photoURL: URL?
    var name: String?
    var price: Int?
    var size: String?
    var quantity: Int?
    
    init() {
        
    }

}

extension Bag: Mappable{
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        
        photoURL <- map["photoURL"]
        name <- map["name"]
        price <- map["price"]
        size <- map["size"]
        quantity <- map["quantity"]

    }
    
    
}
