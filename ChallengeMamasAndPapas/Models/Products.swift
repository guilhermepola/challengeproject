//
//  Products.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
import ObjectMapper

struct Products{
    
    var facets: Facets?
    var hits: [Hits]?
    var pagination: Pagination?
}

extension Products: Mappable{
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        
        facets <- map["facets"]
        hits <- map["hits"]
        pagination <- map["pagination"]
        
    }
    
    
}
