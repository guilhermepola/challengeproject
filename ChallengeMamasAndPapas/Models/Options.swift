//
//  Options.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
import ObjectMapper

struct Options{
    var optionId: Int?
    var label: String?
    var simpleProductSkus: [String]?
    var isInStock: Bool?
    
}

extension Options: Mappable{
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        
        optionId <- map["optionId"]
        label <- map["label"]
        simpleProductSkus <- map["simpleProductSkus"]
        isInStock <- map["isInStock"]
  
    }
    
    
}
