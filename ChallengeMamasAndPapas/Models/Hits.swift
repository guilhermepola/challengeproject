//
//  Hits.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
import ObjectMapper

struct Hits{
    
    var productId: Int?
    var categoryPositions: [String: Int]?
    var sku: String?
    var createdAt: String?
    var typeId: String?
    var name: String?
    var slug: String?
    var brand: String?
    var image: String?
    var smallImage: String?
    var thumbnail: String?
    var giftMessageAvailable: String?
    var giftWrappingAvailable: String?
    var age: [Int]?
    var motherReference: String?
    var description: String?
    var price: Int?
    var specialPrice: Int?
    var specialFromDate: String?
    var newsFromDate: String?
    var newsToDate: String?
    var onlineDateWithStock: String?
    var onlineDate: String?
    var status: Int?
    var visibility: Int?
    var sizeCode: String?
    var sizeCodeId: Int?
    var color: String?
    var colorId: Int?
    var gender: String?
    var collectionCharacter: String?
    var togRating: String?
    var style: String?
    var rating: String?
    var sideImpactProtection: String?
    var cotTypeSize: String?
    var developmentStage: String?
    var colorHex: String?
    var copyAttributes: [String]?
    var parentSku: String?
    var styleColorId: String?
    var ranged: Int?
    var analytics: Analytics?
    var categoryIds: [Int]?
    var categories: [String]?
    var media: [Media]?
    var likes: Int?
    var minPrice: Int?
    var stock: Stock?
    var isInStock: Bool?
    var isInHomeDeliveryStock: Bool?
    var isInClickAndCollectStock: Bool?
    var siblings: [String]?
    var recommended: [String]?
    var upSell: [String]?
    var crossSell: [String]?
    var isClearance: Bool?
    var customAllField: String?
    var discounted: String?
    var configurableAttributes: [ConfigurableAttributes]?
    var availableColors: [String]?
    var simpleType: String?
    var sameColorSiblings: [String]?
    var areAnyOptionsInStock: Bool?
    var stockOfAllOptions: Stock?
    var isInStockFacet: String?
    var areAnyOptionsInStockFacet: String?
    var isInHomeDeliveryStockFacet: String?
    var isInClickAndCollectStockFacet: String?
    var sizesInStock: [String]?
    var sizesInHomeDeliveryStock: [String]?
    var sizesInClickAndCollectStock: [String]?
    var visibleSku: String?
    var stats: Stats?
    var badges: Badges?
    var relatedProductsLookup: [String: Hits]?
    
    
    
    
    
    
}

extension Hits: Mappable{
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        
        productId <- map["productId"]
        categoryPositions <- map["categoryPositions"]
        sku <- map["sku"]
        createdAt <- map["createdAt"]
        typeId <- map["typeId"]
        name <- map["name"]
        slug <- map["slug"]
        brand <- map["brand"]
        image <- map["image"]
        smallImage <- map["smallImage"]
        thumbnail <- map["thumbnail"]
        giftMessageAvailable <- map["giftMessageAvailable"]
        giftWrappingAvailable <- map["giftWrappingAvailable"]
        age <- map["age"]
        motherReference <- map["motherReference"]
        description <- map["description"]
        price <- map["price"]
        specialPrice <- map["specialPrice"]
        specialFromDate <- map["specialFromDate"]
        newsFromDate <- map["newsFromDate"]
        newsToDate <- map["newsToDate"]
        onlineDateWithStock <- map["onlineDateWithStock"]
        onlineDate <- map["onlineDate"]
        status <- map["status"]
        visibility <- map["visibility"]
        sizeCode <- map["sizeCode"]
        sizeCodeId <- map["sizeCodeId"]
        color <- map["color"]
        colorId <- map["colorId"]
        gender <- map["gender"]
        collectionCharacter <- map["collectionCharacter"]
        togRating <- map["togRating"]
        style <- map["style"]
        rating <- map["rating"]
        sideImpactProtection <- map["sideImpactProtection"]
        style <- map["style"]
        cotTypeSize <- map["cotTypeSize"]
        developmentStage <- map["developmentStage"]
        colorHex <- map["colorHex"]
        copyAttributes <- map["copyAttributes"]
        parentSku <- map["parentSku"]
        styleColorId <- map["styleColorId"]
        ranged <- map["ranged"]
        analytics <- map["analytics"]
        categoryIds <- map["categoryIds"]
        categories <- map["categories"]
        media <- map["media"]
        likes <- map["likes"]
        minPrice <- map["minPrice"]
        stock <- map["stock"]
        isInStock <- map["isInStock"]
        isInHomeDeliveryStock <- map["isInHomeDeliveryStock"]
        isInClickAndCollectStock <- map["isInClickAndCollectStock"]
        siblings <- map["siblings"]
        recommended <- map["recommended"]
        upSell <- map["upSell"]
        crossSell <- map["crossSell"]
        isClearance <- map["isClearance"]
        customAllField <- map["customAllField"]
        discounted <- map["discounted"]
        configurableAttributes <- map["configurableAttributes"]
        availableColors <- map["availableColors"]
        simpleType <- map["simpleType"]
        sameColorSiblings <- map["sameColorSiblings"]
        areAnyOptionsInStock <- map["areAnyOptionsInStock"]
        stockOfAllOptions <- map["stockOfAllOptions"]
        isInStockFacet <- map["isInStockFacet"]
        areAnyOptionsInStockFacet <- map["areAnyOptionsInStockFacet"]
        isInHomeDeliveryStockFacet <- map["isInHomeDeliveryStockFacet"]
        isInClickAndCollectStockFacet <- map["isInClickAndCollectStockFacet"]
        sizesInStock <- map["sizesInStock"]
        sizesInHomeDeliveryStock <- map["sizesInHomeDeliveryStock"]
        sizesInClickAndCollectStock <- map["sizesInClickAndCollectStock"]
        visibleSku <- map["visibleSku"]
        stats <- map["stats"]
        badges <- map["badges"]
        relatedProductsLookup <- map["relatedProductsLookup"]



    }
    
    
}
