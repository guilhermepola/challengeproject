//
//  Media.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//


import UIKit
import ObjectMapper

struct Media{
    
    var position: Int?
    var mdediaType: String?
    var src: String?
    var videoURL: String?
}

extension Media: Mappable{
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        position <- map["position"]
        mdediaType <- map["mdediaType"]
        src <- map["src"]
        position <- map["position"]

        
    }
    
    
}
