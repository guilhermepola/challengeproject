//
//  Badges.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
import ObjectMapper

struct Badges{
    var discount: String?
    
}

extension Badges: Mappable{
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        
        discount <- map["discount"]
        
    }
    
    
}
