//
//  HitsAnalytics.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
import ObjectMapper

struct Analytics{
    
    var name: String?
    var designer: String?
    var color: String?
    
}

extension Analytics: Mappable{
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        
        name <- map["name"]
        designer <- map["designer"]
        color <- map["color"]
        
    }
    
    
}
