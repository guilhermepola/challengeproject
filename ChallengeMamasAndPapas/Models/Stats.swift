//
//  Stats.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//


import UIKit
import ObjectMapper

struct Stats{
    
    var itemRevenue: Int?
    var listView: Int?
    var detailView: Int?
    var addsToCart: Int?
    var checkOuts: Int?
    var uniquePurchases: Int?
    var listToDetail: Double?
}

extension Stats: Mappable{
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        
        itemRevenue <- map["itemRevenue"]
        listView <- map["listView"]
        detailView <- map["detailView"]
        addsToCart <- map["addsToCart"]
        checkOuts <- map["checkOuts"]
        uniquePurchases <- map["uniquePurchases"]
        listToDetail <- map["listToDetail"]

    }
    
    
}
