//
//  Pagination.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//


import UIKit
import ObjectMapper

struct Pagination{
    
    var totalHits: Int?
    var totalPages: Int?
}

extension Pagination: Mappable{
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        
        totalHits <- map["totalHits"]
        totalPages <- map["totalPages"]
        
    }
    
    
}
