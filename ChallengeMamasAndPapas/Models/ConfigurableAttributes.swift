//
//  ConfigurableAttributes.swift
//  ChallengeMamasAndPapas
//
//  Created by Guilherme Pola on 11/8/17.
//  Copyright © 2017 Guilherme Pola. All rights reserved.
//

import UIKit
import ObjectMapper

struct ConfigurableAttributes{
    var code: String?
    var options: [Options]?
    
}

extension ConfigurableAttributes: Mappable{
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        code <- map["code"]
        options <- map["options"]

        
        
    }
    
    
}
